import ToDo from "./ToDo";
const ShowList = (props) =>{
    const {toDoList} = props;
    return(
        <div>
            <ul>
             {toDoList.map((todo,index) => (<ToDo key={index} todo={todo} />))}
           </ul>
        </div>
    )
}

export default ShowList;