import { useState } from 'react';
const AddToDo = (props) => {
    const { addToDo } = props;
    const [inputData, setInputData] = useState('');

    const handleInputChange = (event) => {
        setInputData(event.target.value);
    };

    const handleSubmit = () => {
        addToDo(inputData);
        setInputData('');
    };
    return (
        <div>
            <input
                type="text"
                value={inputData}
                onChange={handleInputChange}
            />
            <button onClick={handleSubmit}>提交</button>
        </div>
    )

}

export default AddToDo;