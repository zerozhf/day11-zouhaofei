
import { useState } from 'react';
import ShowList from './component/ShowList';
import AddToDo from './component/AddToDo';
import './App.css';

function App() {
  const [toDoList,setToDoList ]= useState([]);
  const addToDo = (todo) => {
    setToDoList([...toDoList,todo]);
  }
  return (
    <div className="App">
      <h1>ToDo List</h1>
      <ShowList toDoList = {toDoList}/>
      <AddToDo addToDo = {addToDo} />
    </div>
  );
}

export default App;
