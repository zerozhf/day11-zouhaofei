# Daily Report (2023/07/24)

## O

### Code Review

#### The logic of the data mapping of Request and Response is written in Service, and when performing API testing, use Request to start testing from Controller.

### html CSS

#### I learned html and CSS a long time ago, but I don’t use them often, and I’m not very familiar with many grammars. When using them, I need to check the information and read official documents frequently, so I just use this learning opportunity to re-familiarize with HTML and CSS.

### React

#### In the previous projects, Vue was used on the front end, so I have almost never been in touch with React. The first time I came into contact with React today, I feel that React and Vue use many of the same places. I can quickly accept this part, but the syntax of React and Vue is still quite different. I will go to the official website of React when I have time to get familiar with yufu of React.

##### 

## R

#### meaningful

## I

#### The most meaningful activity is React

## D

#### When learning React, compare and learn with Vue to improve learning efficiency